x = [12, 287, 5.6, 19.2, 29, 21, 167.2]
y = [189, 1.4, 6.3, 17, 286]

def ortalama(p_liste):
    toplam = 0
    for i in p_liste:
        toplam += i 
    return toplam / len(p_liste)
    
print(round(ortalama(x), 2))
print(round(ortalama(y), 2))

def enBuyukOge(p_liste):
    ebo = p_liste[0]
    i = 0
    while i < len(p_liste):
        if p_liste[i] > ebo:
            ebo = p_liste[i]
        i += 1 
    return ebo 
    
print(enBuyukOge(y))
