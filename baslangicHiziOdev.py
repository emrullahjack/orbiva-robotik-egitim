baslangictaHizlanmaAktif = True
suanki_hiz = 0.0
baslangicHedefHizi = 2.0

def ciktiyiFormatla(x):
    gecici = str(x)
    sonuc = ""
    if len(gecici) > 3:
        i = 0
        while i < 3:
            sonuc += gecici[i]
            i += 1
    else:
        sonuc = gecici
    return sonuc

print("+ Robot calistirildi")

while suanki_hiz < baslangicHedefHizi:
    suanki_hiz += 0.1 
    print("suanki hiz: ", ciktiyiFormatla(suanki_hiz), "m/s")

print("+ Hedeflenen baslangic hizina erisildi")


